## Como contribuir
Estou muito feliz por você estar lendo isso, porque precisamos de desenvolvedores voluntários para ajudar no desenvolvimento deste projeto.

Se você ainda não entrou nos grupos deste projeto, pedimos que faça antes de meter a mão na massa.

#### Informações importantes :

O grupo desaparecidos.me visa colaborar na busca de pessoas desaparecidas, não cobramos pelos serviços prestado para os nossos usuários. 

## Testando

* O Projeto mantem sua cobertura de testes acima de 80%
* Utilizamos `unittest` para os testes unitários e de integração

## Enviando alterações

Por favor, envie um Pull Request ao desaparecidos.me com uma lista clara do que você fez (leia mais sobre pull requests). Quando você envia uma solicitação pull, nós o avaliamos com base nos pilares de qualidades _unit, integration, system_. Podemos sempre usar mais cobertura de teste. Por favor, siga nossas convenções de codificação (abaixo) e certifique-se de que todos os seus commits são atômicos (um recurso por commit).

Sempre escreva uma mensagem de log clara para seus commits. Mensagens de uma linha são boas para pequenas mudanças, mas mudanças maiores devem ser assim:

```bash
$ git commit -m "Um breve resumo do commit
``` 
>
> Um parágrafo descrevendo o que mudou e seu impacto. "
Convenções de codificação
Comece a ler o nosso código e você vai pegar o jeito dele. Nós otimizamos a legibilidade