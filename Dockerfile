FROM python:3.6.5
MAINTAINER Leco "desaparecidos.me@gmail.com"
# COPY APP
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]