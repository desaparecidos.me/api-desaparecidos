import os

from flask import Flask
from flask_restful import Api
from redis import Redis

from configuration.celery_configuration import celery
from configuration.mongodb_configuration import database, create_index
from resources.auth_resource import AuthResource
from resources.disappeared_list_resource import DisappearedListResource
from resources.disappeared_resource import DisappearedResource
from resources.homeless_list_resource import HomelessListResource
from resources.homeless_resource import HomelessResource
from resources.missing_resource import MissingResource
from resources.missing_sighted_resource import MissingSightedResource
from resources.remember_resource import RememberResource
from resources.sighted_resource import SightedResource
from resources.user_resource import UserResource
from resources.users_resource import UsersResource
from configuration.health_check_configuration import health

app = Flask(__name__)
health.init_app(app=app, path="/health")

app.secret_key = "@P1-DES@P@R3C1D0$"

# DATABASE CONFIGURATION
app.config["MONGO_URI"] = os.environ.get('MONGODB_URI')
database.init_app(app)
create_index(database.db)

# CELERY CONFIGURATION
app.config['CELERY_BROKER_URL'] = os.environ.get('REDIS_URL')
app.config['CELERY_RESULT_BACKEND'] = os.environ.get('REDIS_URL')
celery.init_app(app)

# ROUTES CONFIGURATION
api = Api(app)

api.add_resource(AuthResource, '/auth', endpoint="authentication")
api.add_resource(RememberResource, '/auth/remember')

api.add_resource(UsersResource, '/users', endpoint="users")
api.add_resource(UserResource, '/user/<string:_id>', endpoint="user")

api.add_resource(DisappearedResource, '/user/<string:user_id>/disappeared', endpoint="disappeared")
api.add_resource(DisappearedListResource, '/disappeared', endpoint="disappeared_list")

api.add_resource(MissingResource, '/missing/<string:_id>', endpoint="missing")
api.add_resource(MissingSightedResource, '/missing/<string:_id>/sighted', endpoint="missing_sighted")
api.add_resource(SightedResource, '/sighted/<string:_id>', endpoint="sighted")

api.add_resource(HomelessListResource, '/homeless', endpoint="homeless_list")
api.add_resource(HomelessResource, '/homeless/<string:_id>', endpoint="homeless")

if __name__ == '__main__':
    app.run(port=5001)
