import re
from datetime import datetime, date


def string_to_date(value):
    return datetime.combine(datetime.strptime(value, '%d/%m/%Y').date(), datetime.min.time())


def date_to_string(value):
    return datetime.strftime(value, '%d/%m/%Y')


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def was_child(birth_date, date_of_disappearance):
    return date_of_disappearance.year - birth_date.year - (
            (date_of_disappearance.month, date_of_disappearance.day) < (birth_date.month, birth_date.day)) < 18


def is_valid_format(value):
    return re.match("\d{2}\/\d{2}\/\d{4}", value)
