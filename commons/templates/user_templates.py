REMEMBER_PASSWORD = """
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<div style="margin: 5px auto; text-align:center; padding: 10px;font-family: 'Raleway', sans-serif;">
    <h1>Desaparecidos.me</h1>
    <p>Geramos uma nova senha para você.</p>
    <p style="background-color: #FF9800;padding: 5px;border-radius: 5px; color: white; font-weight: bold">{}</p>
</div>
"""

SIGHTED_MISSING = """
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<div style="margin: 5px auto; text-align:center; padding: 10px;
                       font-family: 'Raleway', sans-serif;">
    <h1>Desaparecidos.me</h1>
    <p>Alguém informou que a pessoa desaparecida foi avistada, acesse a platafora e veja onde.</p>
    <h3>Observação </h3>
    <p style="background-color: #0097D6; padding: 5px;border-radius: 5px;
                        color: black; font-weight: bold">{}</p>
</div>
"""
