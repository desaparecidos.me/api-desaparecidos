import os
from flask_httpauth import HTTPBasicAuth

USER = {
    "username": os.environ.get("BASIC_AUTH_USERNAME"),
    "password": os.environ.get("BASIC_AUTH_PASSWORD")
}

auth = HTTPBasicAuth()


@auth.verify_password
def verify(username, password):
    return USER.get("username") == username and USER.get("password") == password
