import configuration.mongodb_configuration as mongodb
import configuration.redis_configuration as redis

from healthcheck import HealthCheck

health = HealthCheck()

health.add_check(mongodb.mongodb_available)
health.add_check(redis.redis_available)
