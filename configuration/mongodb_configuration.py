import os
import pymongo
from flask_pymongo import PyMongo

database = PyMongo()


def mongodb_available():
    server = pymongo.MongoClient(os.environ.get("MONGODB_URI"))
    server.server_info()
    return True, "mongodb ok"


def create_index(db):
    if 'user_email' not in db.users.index_information():
        db.users.create_index([('email', 1)], name='user_email', unique=True)

    if 'homeless_slug' not in db.homeless.index_information():
        db.homeless.create_index([('slug', 1)], name='homeless_slug', unique=True)

    if 'disappeared_slug' not in db.disappeared.index_information():
        db.disappeared.create_index([('slug', 1)], name='disappeared_slug', unique=True)

    if 'missing_id' not in db.sighted.index_information():
        db.sighted.create_index([('disappeared_id', 1)], name='missing_id')

    if 'search_engine' not in db.disappeared.index_information():
        db.disappeared.create_index([("$**", pymongo.TEXT)], name='search_engine')
