import os

from redis import Redis


def redis_available():
    client = Redis.from_url(os.environ.get("REDIS_URL"))
    client.info()
    return True, "redis ok"
