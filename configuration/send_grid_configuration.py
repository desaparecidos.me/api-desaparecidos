import os

import sendgrid
from sendgrid import Email
from sendgrid.helpers.mail import Content, Mail

from configuration.loggin_configuration import logger

send_grid = sendgrid.SendGridAPIClient(apikey=os.environ.get("SENDGRID_API_KEY"))
from_mail = Email(os.environ.get('DESAPARECIDOS_ME'))


def send_html(to, subject, html):
    try:
        to_email = Email(to)
        content_type = Content("text/html", html)
        mail = Mail(from_mail, subject, to_email, content_type)
        send_grid.client.mail.send.post(request_body=mail.get())
        logger.info("Send email with success")
    except Exception as e:
        logger.error("Error during send email", e)
