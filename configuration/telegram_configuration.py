import os

from telegram import bot, ParseMode


def send_message_markdown_format(message):
    telegram = bot.Bot(os.environ.get("TELEGRAM_KEY"))

    telegram.send_message(parse_mode=ParseMode.MARKDOWN,
                          chat_id=os.environ.get("TELEGRAM_CHAT_ID"),
                          text=message)
