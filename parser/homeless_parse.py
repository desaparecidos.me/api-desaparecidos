from commons.fomat_date import is_valid_format, string_to_date
from flask_restful import reqparse


def homeless_json():
    parse = reqparse.RequestParser()
    parse.add_argument("name", type=str, required=True, help="Name is required")
    parse.add_argument("nickname", type=str)
    parse.add_argument("birth_date", type=format_date, required=True, help="Birth date is required")
    parse.add_argument("geometry", action="append", required=True, help="Geometry is required")
    parse.add_argument("eyes", choices=('green', 'blue', 'brown', 'black', 'dark_brown'), required=True,
                       help="Eyes is required")

    parse.add_argument("gender", choices=('male', 'female'), required=True, help="Gender is required")
    parse.add_argument("hair", choices=('black', 'brown', 'redhead', 'blond'), required=True, help="Hair is required")
    parse.add_argument("skin", choices=('white', 'brown', 'yellow', 'black'), required=True, help="Skin is required")
    parse.add_argument("photo", type=str, required=True, help="Photo is required")
    return parse.parse_args()


def format_date(value):
    if not is_valid_format(value):
        raise ValueError("Invalid format to birth date")
    return string_to_date(value)

