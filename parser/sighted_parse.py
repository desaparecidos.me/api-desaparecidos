from flask_restful import reqparse


def sighted_json():
    parse = reqparse.RequestParser()
    parse.add_argument("geometry", action="append", required=True, help="Geometry is required")
    parse.add_argument("observation", type=str, required=True, help="Observation is required")
    return parse.parse_args()
