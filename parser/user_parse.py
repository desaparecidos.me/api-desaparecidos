from flask_restful import reqparse


def user_json():
    parser = reqparse.RequestParser()
    parser.add_argument("name", required=True, type=str, help="Name is required")
    parser.add_argument("email", required=True, type=str, help="Email is required")
    parser.add_argument("accept_terms", required=True, type=bool, help="Accept terms is required")
    parser.add_argument("password", type=str)
    parser.add_argument("phone", type=str)
    parser.add_argument("cell_phone", type=str)
    parser.add_argument("avatar", type=str)

    return parser.parse_args()


def password_json():
    parser = reqparse.RequestParser()
    parser.add_argument("password", required=True, type=str, help="Password is required")
    return parser.parse_args()


def authentication_json():
    parser = reqparse.RequestParser()
    parser.add_argument("email", required=True, type=str, help="Email is required")
    parser.add_argument("password", required=True, type=str, help="Password is required")
    return parser.parse_args()
