import pymongo

from commons.fomat_date import date_to_string, calculate_age
from configuration.mongodb_configuration import database


def create_disappeared(missing):
    database.db.disappeared.save(missing)


def find_by_id(_id):
    return database.db.disappeared.find_one({"_id": _id})


def count():
    return database.db.disappeared.find({"status": "disappeared"}).count()


def find_by_user_id(user_id):
    return database.db.disappeared.find({"user_id": user_id})


def group_by_gender():
    result = {
        "male": 0,
        "female": 0
    }

    pipeline = [{"$group": {"_id": "$gender", "sum": {"$sum": 1}}}]
    disappeared = database.db.disappeared.aggregate(pipeline)

    for item in disappeared:
        if 'male' == item["_id"]:
            result["male"] = item["sum"]
        else:
            result["female"] = item["sum"]

    return result


def group_by_child():
    return database.db.disappeared.find({"was_child": True}).count()


def group_by_years():
    pipeline = [{"$group": {"_id": {"$year": "$date_of_disappearance"}, "sum": {"$sum": 1}}}]
    disappeared = database.db.disappeared.aggregate(pipeline)
    return [{"year": item["_id"], "value": item["sum"]} for item in disappeared]


def pagination(page, limit):
    return database.db.disappeared.find({
        "status": "disappeared"
    }).sort("registration_date", pymongo.DESCENDING).skip(int(page) * limit).limit(limit)


def search(text):
    return database.db.disappeared.find({"$text": {"$search": text, "$caseSensitive": False}})


def group_by_geometry():
    pipeline = [{"$group": {"_id": "$geometry", "sum": {"$sum": 1}}}]
    result = database.db.disappeared.aggregate(pipeline)
    return [{
        "geometry": location["_id"],
        "quantity": location["sum"],
    } for location in result]


def remove_disappeared(_id):
    return database.db.disappeared.delete_one({"_id": _id})


def json(cursor):
    return {
        "_id": str(cursor["_id"]),
        "name": cursor["name"],
        "nickname": cursor["nickname"],
        "birth_date": date_to_string(cursor["birth_date"]),
        "age": calculate_age(cursor["birth_date"]),
        "gender": cursor["gender"],
        "photo": cursor["photo"],
        "height": cursor["height"],
        "user_id": cursor["user_id"],
        "clothes": cursor["clothes"],
        "date_of_disappearance": date_to_string(cursor["date_of_disappearance"]),
        "status": cursor["status"],
        "eyes": cursor["eyes"],
        "event_report": cursor["event_report"],
        "hair": cursor["hair"],
        "skin": cursor["skin"],
        "tattoo": cursor["tattoo"],
        "tattoo_description": cursor["tattoo_description"],
        "scar": cursor["scar"],
        "scar_description": cursor["scar_description"],
        "geometry": ','.join(cursor["geometry"])
    } if cursor else {}
