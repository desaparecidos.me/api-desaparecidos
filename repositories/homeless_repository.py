from commons.fomat_date import date_to_string, calculate_age
from configuration.mongodb_configuration import database


def json(cursor):
    return {
        "_id": str(cursor['_id']),
        "name": cursor["name"],
        "nickname": cursor["nickname"],
        "birth_date": date_to_string(cursor["birth_date"]),
        "age": calculate_age(cursor["birth_date"]),
        "geometry": cursor["geometry"],
        "eyes": cursor["eyes"],
        "gender": cursor["gender"],
        "hair": cursor["hair"],
        "skin": cursor["skin"],
        "photo": cursor["photo"]
    } if cursor else {}


def create_homeless(data):
    database.db.homeless.save(data)


def find_all():
    return database.db.homeless.find({})


def find_by_id(_id):
    return database.db.homeless.find_one({"_id": _id})


def count():
    return database.db.homeless.find().count()


def group_by_gender():
    result = {
        "male": 0,
        "female": 0
    }

    pipeline = [{"$group": {"_id": "$gender", "sum": {"$sum": 1}}}]
    homeless = database.db.homeless.aggregate(pipeline)

    for item in homeless:
        if 'male' == item["_id"]:
            result["male"] = item["sum"]
        else:
            result["female"] = item["sum"]

    return result
