from commons.fomat_date import date_to_string
from configuration.mongodb_configuration import database


def create_sighted(sighted):
    database.db.sighted.save(sighted)


def get_sighted_by_missing_id(disappeared_id):
    return database.db.sighted.find({"disappeared_id": disappeared_id})


def json(cursor):
    return {
        "_id": str(cursor["_id"]),
        "disappeared_id": cursor["disappeared_id"],
        "registration_date": date_to_string(cursor["registration_date"]),
        "geometry": cursor["geometry"],
        "observation": cursor["observation"]
    } if cursor else {}


def find_by_id(_id):
    return database.db.sighted.find_one({"_id": _id})
