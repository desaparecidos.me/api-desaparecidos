from configuration.mongodb_configuration import database


def json(cursor):
    return {
        "_id": str(cursor["_id"]),
        "name": cursor["name"],
        "email": cursor["email"],
        "phone": cursor["phone"],
        "cell_phone": cursor["cell_phone"],
        "avatar": cursor["avatar"]
    } if cursor else {}


def find_by_id(_id):
    return database.db.users.find_one({"_id": _id})


def create_user(data):
    return database.db.users.save(data)


def delete_user(_id):
    return database.db.users.delete_one({"_id": _id})


def get_all():
    return database.db.users.find({})


def find_by_email_and_password(email, password):
    return database.db.users.find_one({"email": email, "password": password})


def find_by_email(email):
    return database.db.users.find_one({"email": email})
