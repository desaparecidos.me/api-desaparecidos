from werkzeug.exceptions import BadRequest

from configuration.basic_authentication import auth
import parser.user_parse as parse
import services.user_service as user_service

from flask_restful import Resource

from configuration.loggin_configuration import logger


class AuthResource(Resource):

    @auth.login_required
    def post(self):
        try:
            data = parse.authentication_json()
            user = user_service.auth(data["email"], data["password"])
            if user:
                logger.info("User %s authorized!", data["email"])
                return {"user": user}, 200
            else:
                logger.info("User %s has not found!", data["email"])
                return {"message": "User not found!"}, 404
        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Request error fields required"}, 400
        except Exception as e:
            logger.error("Error during authentication user", e)
            return {"message": "Error during authentication user"}, 500
