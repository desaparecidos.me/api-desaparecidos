from flask import request
from flask_restful import Resource

import services.disappeared_service as disappeared_service
from configuration.loggin_configuration import logger


class DisappearedListResource(Resource):

    def get(self):
        try:
            if 'page' in request.args:
                return disappeared_service.pagination(int(request.args["page"])), 200
            if 'count' in request.args:
                return {"count": disappeared_service.count()}, 200
            if 'search' in request.args:
                return {"disappeared": disappeared_service.search(request.args["search"])}, 200
            if 'geometry' in request.args:
                return {"disappeared": disappeared_service.group_by_geometry()}, 200
            if 'info' in request.args:
                return {'disappeared': disappeared_service.info()}, 200
        except Exception as e:
            logger.error("Error during find disappeared %s", e)
            return {"message": "Error during find disappeared"}, 500
