from flask import url_for
from flask_restful import Resource
from pymongo.errors import DuplicateKeyError
from werkzeug.exceptions import BadRequest

from configuration.basic_authentication import auth
import services.disappeared_service as disappeared_service
from configuration.loggin_configuration import logger
from parser import missing_parse
from services import user_service


class DisappearedResource(Resource):

    @auth.login_required
    def post(self, user_id):
        try:
            data = missing_parse.missing_json()
            if user_service.has_id(user_id):
                disappeared_service.create_disappeared(user_id, data)
                logger.info("Missing created with success!")

                return {"uri": url_for("missing", _id=str(data["_id"]))}, 201
            else:
                logger.error("User %s not found!", user_id)
                return {"message": "User not found"}, 404
        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Request error fields required"}, 400
        except DuplicateKeyError as e:
            logger.error("Error during create missing %s has registered", e)
            return {"message": "Error conflict, missing has registered!"}, 409
        except Exception as e:
            logger.error("Error during create missing %s", e)
            return {"message": "Error during create missing"}, 500

    @auth.login_required
    def get(self, user_id):
        try:
            if user_service.has_id(user_id):
                disappeared = disappeared_service.find_by_user_id(user_id)
                if disappeared:
                    return {"disappeared": disappeared}, 200
                else:
                    return {"disappeared": {}}, 404
            else:
                logger.error("User %s not found!", user_id)
                return {"message": "User not found"}, 404
        except Exception as e:
            logger.error("Error during create missing %s", e)
            return {"message": "Error during create missing"}, 500
