from flask import url_for, request
from flask_restful import Resource
from pymongo.errors import DuplicateKeyError
from werkzeug.exceptions import BadRequest

from configuration.basic_authentication import auth
import parser.homeless_parse as parse
import services.homeless_service as homeless_service
from configuration.loggin_configuration import logger


class HomelessListResource(Resource):

    @auth.login_required
    def post(self):
        try:
            data = parse.homeless_json()
            homeless_service.create_homeless(data)
            logger.info("Homeless created!")

            return {"uri": url_for('homeless', _id=str(data['_id']))}, 201
        except DuplicateKeyError as e:
            logger.error("Error during create homeless has registered", e)
            return {"message": "Error conflict, homeless has registered!"}, 409
        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Json malformed"}, 400
        except Exception as e:
            logger.error("Error during create homeless %s", e)
            return {"message": "Error during create homeless"}, 500

    @auth.login_required
    def get(self):
        try:
            if 'info' in request.args:
                return {'homeless': homeless_service.info()}, 200

            homeless = homeless_service.find_all()
            logger.info("Get all homeless, total %s", str(len(homeless)))
            return {"homeless": homeless}, 200
        except Exception as e:
            logger.error("Error during get homeless %s", e)
            return {"message": "Error during get homeless"}, 500
