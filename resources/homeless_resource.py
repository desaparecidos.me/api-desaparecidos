from flask_restful import Resource

import services.homeless_service as homeless_service
from configuration.loggin_configuration import logger
from configuration.basic_authentication import auth


class HomelessResource(Resource):

    @auth.login_required
    def get(self, _id):
        try:
            homeless = homeless_service.find_by_id(_id)
            if homeless:
                logger.info("Find homeless by id %s", _id)
                return {"homeless": homeless}, 200
            else:
                logger.info("Homeless not found %s", _id)
                return {"homeless": {}}, 404

        except Exception as e:
            logger.error("Error during get homeless %s", e)
            return {"message": "Error during get homeless"}, 500
