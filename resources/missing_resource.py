from flask import url_for
from flask_restful import Resource

from configuration.basic_authentication import auth
import services.disappeared_service as disappeared_service
import services.user_service as user_service
from configuration.loggin_configuration import logger
from parser.missing_parse import missing_json


class MissingResource(Resource):

    @auth.login_required
    def put(self, _id):
        try:
            data = missing_json()
            if data["user_id"] and user_service.has_id(data["user_id"]):
                if disappeared_service.has_id(_id):
                    disappeared_service.update_disappeared(_id, data)
                    logger.info("Missing %s update with success", _id)

                    return {"uri": url_for('missing', _id=_id)}, 200
                else:
                    disappeared_service.create_disappeared(data["user_id"], data)
                    logger.info("Missing %s created with success", data["_id"])

                    return {"uri": url_for("missing", _id=str(data["_id"]))}, 201
            else:
                logger.error("User id %s not found", _id)
                return {"message": "User not found!"}, 404
        except Exception as e:
            logger.error("Error during update missing %s", e)
            return {"message": "Error during update missing"}, 500

    @auth.login_required
    def get(self, _id):
        try:
            disappeared = disappeared_service.find_by_id(_id)
            if disappeared:
                return {"missing": disappeared}
            else:
                logger.error("Missing %s not found", _id)
                return {"message": "Missing not found!"}, 404
        except Exception as e:
            logger.error("Error during get missing %s", e)
            return {"message": "Error during get missing"}, 500

    @auth.login_required
    def delete(self, _id):
        try:
            if disappeared_service.has_id(_id):
                disappeared_service.remove_disappeared(_id)
                logger.info("Missing %s removed with success", _id)
                return {}, 204
            else:
                logger.error("Missing %s not found", _id)
                return {"message": "Missing not found!"}, 404
        except Exception as e:
            logger.error("Error during delete missing %s", e)
            return {"message": "Error during delete missing"}, 500
