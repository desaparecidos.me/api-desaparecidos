from flask import url_for
from flask_restful import Resource

import parser.sighted_parse as sighted_parser
from configuration.basic_authentication import auth
import services.disappeared_service as disappeared_service
import services.sighted_service as sighted_service
from configuration.loggin_configuration import logger


class MissingSightedResource(Resource):

    @auth.login_required
    def post(self, _id):
        try:
            data = sighted_parser.sighted_json()
            if disappeared_service.has_id(_id):
                sighted_service.create_sighted(_id, data)
                logger.info("Sighted create with success, missing %s", _id)

                return {"uri": url_for('sighted', _id=str(data["_id"]))}, 201
            else:
                logger.info("Missing %s not found", _id)
                return {"message": "Missing not found"}, 404
        except Exception as e:
            logger.error("Error during sighted missing", e)
            return {"message": "Error during sighted missing"}, 500

    @auth.login_required
    def get(self, _id):
        try:
            if disappeared_service.has_id(_id):
                result = sighted_service.get_sighted_by_missing_id(_id)
                return {"sighted": result}, 200
            else:
                logger.info("Missing %s not found", _id)
                return {"message": "Missing not found"}, 404
        except Exception as e:
            logger.error("Error during sighted missing", e)
            return {"message": "Error during get sighted missing"}, 500
