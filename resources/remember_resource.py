from werkzeug.exceptions import BadRequest

import services.user_service as user_service
from flask_restful import Resource, reqparse
from configuration.basic_authentication import auth
from configuration.loggin_configuration import logger


class RememberResource(Resource):

    @auth.login_required
    def patch(self):
        try:
            parse = reqparse.RequestParser()
            parse.add_argument("email", required=True, type=str, help="Email is required")
            data = parse.parse_args()
            user = user_service.find_by_email(data["email"])
            if user:
                user_service.remember_password(user)
                logger.info("Send email to %s with new password!", data["email"])
                return {"message": "Send email with new password"}, 200
            else:
                logger.info("User %s does not found!", data["email"])
                return {"message": "User not found!"}, 404
        except BadRequest as e:
            logger.error("Json malformed to remember password %s", e)
            return {"message": "Json malformed to remember password"}, 400
        except Exception as e:
            logger.error("Error during remember password user  %s", e)
            return {"message": "Error during remember password user"}, 500
