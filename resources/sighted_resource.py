from flask_restful import Resource

from configuration.basic_authentication import auth
import services.sighted_service as sighted_service
from configuration.loggin_configuration import logger


class SightedResource(Resource):

    @auth.login_required
    def get(self, _id):
        try:
            sighted = sighted_service.find_by_id(_id)
            if sighted:
                return {"sighted": sighted}, 200
            else:
                logger.info("Missing %s not found", _id)
                return {"sighted": {}}, 404
        except Exception as e:
            logger.error("Error during sighted missing", e)
            return {"message": "Error during get sighted missing"}, 500
