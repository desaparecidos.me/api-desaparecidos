from bson.errors import InvalidId
from flask import url_for
from flask_restful import Resource
from pymongo.errors import DuplicateKeyError
from werkzeug.exceptions import BadRequest

import parser.user_parse as user_parse
import services.user_service as user_service
from configuration.basic_authentication import auth
from configuration.loggin_configuration import logger


class UserResource(Resource):

    @auth.login_required
    def get(self, _id):
        try:
            result = user_service.find_by_id(_id)
            if result:
                return {"user": result}, 200
            else:
                logger.info("User %s does not found!", _id)
                return {"user": {}}, 404
        except InvalidId as e:
            logger.error("Invalid id  %s", e)
            return {"message": "Error the id {}, informed is invalid".format(_id)}, 400
        except Exception as e:
            logger.error("Error during get user %s", e)
            return {"message": "Error during created user"}, 500

    @auth.login_required
    def put(self, _id):
        try:
            data = user_parse.user_json()
            if user_service.has_id(_id):
                user_service.update_user(_id, data)
                logger.info("User updated with success!")

                return {"uri": url_for('user', _id=_id)}, 200
            else:
                user_service.create_user(data)
                logger.info("User created with success!")

                return {"uri": url_for('user', _id=str(data["_id"]))}, 201
        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Request error fields required"}, 400
        except DuplicateKeyError as e:
            logger.error("Error during update user %s email has registered", e)
            return {"message": "Error conflict, email has registered!"}, 409
        except Exception as e:
            logger.error("Error during update user %s", e)
            return {"message": "Error during update user"}, 500

    @auth.login_required
    def delete(self, _id):
        try:
            if user_service.has_id(_id):
                user_service.delete_user(_id)
                logger.info("User %s removed with success!", _id)
                return {}, 204
            else:
                logger.info("User %s has not found!", _id)
                return {"message": "User not found!"}, 404
        except Exception as e:
            logger.error("Error during delete user %s", e)
            return {"message": "Error during delete user"}, 500

    @auth.login_required
    def patch(self, _id):
        try:
            data = user_parse.password_json()
            if user_service.has_id(_id):
                user_service.update_password(_id, data)
                logger.info("Password updated with success!")

                return {"uri": url_for('user', _id=_id)}, 200
            else:
                logger.info("User %s has not found!", _id)
                return {"message": "User not found!"}, 404
        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Request error fields required"}, 400
        except Exception as e:
            logger.error("Error during update password %s", e)
            return {"message": "Error during update password"}, 500
