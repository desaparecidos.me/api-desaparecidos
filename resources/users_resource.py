from flask_restful import Resource
from pymongo.errors import DuplicateKeyError
from werkzeug.exceptions import BadRequest

import parser.user_parse as user_parse
import services.user_service as user_service
from configuration.basic_authentication import auth
from configuration.loggin_configuration import logger


class UsersResource(Resource):
    @auth.login_required
    def post(self):
        try:
            data = user_parse.user_json()
            user_service.create_user(data)
            logger.info("User created with success!")

            return {"uri": "/user/{}".format(str(data["_id"]))}, 201

        except BadRequest as e:
            logger.error("Request error fields required %s", e)
            return {"message": "Request error fields required"}, 400
        except DuplicateKeyError as e:
            logger.error("Error during update user %s email was registered", e)
            return {"message": "Error conflict, email was registered!"}, 409
        except Exception as e:
            logger.error("Error during create user %s", e)
            return {"message": "Error during create user"}, 500

    @auth.login_required
    def get(self):
        try:
            result = user_service.get_all()
            return {"users": result}, 200
        except Exception as e:
            logger.error("Error during update user %s", e)
            return {"message": "Error during update user"}, 500
