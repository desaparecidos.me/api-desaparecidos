import os
from datetime import datetime

from bson import ObjectId
from slugify import slugify

import repositories.disappeared_repository as disappeared_repository
from commons.fomat_date import was_child


def create_disappeared(user_id, missing):
    missing["user_id"] = user_id

    missing["scar"] = missing["scar_description"] is not None
    missing["registration_date"] = datetime.now()
    missing["tattoo"] = missing["tattoo_description"] is not None
    missing["was_child"] = was_child(missing["birth_date"], missing["date_of_disappearance"])

    missing["slug"] = slugify('-'.join([
        user_id,
        missing["name"],
        str(missing['birth_date']),
        missing['eyes'],
        missing['skin'],
        missing['hair']
    ]))

    disappeared_repository.create_disappeared(missing)


def find_by_id(_id):
    return disappeared_repository.json(disappeared_repository.find_by_id(ObjectId(_id)))


def pages():
    return disappeared_repository.count()


def count():
    return disappeared_repository.count()


def has_id(_id):
    return disappeared_repository.find_by_id(ObjectId(_id)) is not None


def find_by_user_id(user_id):
    return [disappeared_repository.json(item) for item in disappeared_repository.find_by_user_id(user_id)]


def pagination(page):
    limit = int(os.environ.get("LIMIT", 20))
    return {
        "disappeared": [disappeared_repository.json(item) for item in disappeared_repository.pagination(page, limit)],
        "pages": round(disappeared_repository.count() / limit)
    }


def info():
    return {
        "total": disappeared_repository.count(),
        "gender": disappeared_repository.group_by_gender(),
        "child": disappeared_repository.group_by_child(),
        "years": disappeared_repository.group_by_years()
    }


def search(text):
    return [disappeared_repository.json(item) for item in disappeared_repository.search(text)]


def group_by_geometry():
    return disappeared_repository.group_by_geometry()


def remove_disappeared(_id):
    return disappeared_repository.remove_disappeared(ObjectId(_id))


def update_disappeared(_id, data):
    missing = disappeared_repository.find_by_id(ObjectId(_id))

    missing["name"] = data["name"]
    missing["nickname"] = data["nickname"]
    missing["height"] = data["height"]
    missing["clothes"] = data["clothes"]
    missing["gender"] = data["gender"]
    missing["eyes"] = data["eyes"]
    missing["birth_date"] = data["birth_date"]
    missing["event_report"] = data["event_report"]
    missing["hair"] = data["hair"]
    missing["skin"] = data["skin"]
    missing["geometry"] = data["geometry"]
    missing["date_of_disappearance"] = data["date_of_disappearance"]
    missing["status"] = data["status"]
    missing["tattoo_description"] = data["tattoo_description"]
    missing["scar_description"] = data["scar_description"]
    missing["photo"] = data["photo"]
    missing["user_id"] = data["user_id"]
    missing["scar"] = data["scar_description"] is not None
    missing["tattoo"] = data["tattoo_description"] is not None
    missing["was_child"] = was_child(data["birth_date"], data["date_of_disappearance"])
    missing["slug"] = slugify('-'.join([
        data["user_id"],
        data["name"],
        str(data['birth_date'].date),
        data['eyes'],
        data['skin'],
        data['hair']
    ]))

    disappeared_repository.create_disappeared(missing)
