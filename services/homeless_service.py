from datetime import datetime

from bson import ObjectId
from slugify import slugify

import repositories.homeless_repository as homeless_repository
import services.notification_service as notification_service
from commons.fomat_date import date_to_string


def info():
    return {
        "total": homeless_repository.count(),
        "gender": homeless_repository.group_by_gender()
    }


def find_all():
    return [homeless_repository.json(item) for item in homeless_repository.find_all()]


def find_by_id(_id):
    return homeless_repository.json(homeless_repository.find_by_id(ObjectId(_id)))


def create_homeless(data):
    data["registration"] = datetime.now()
    data["slug"] = slugify(
        '-'.join([
            data["name"],
            str(data['birth_date']),
            data['eyes'],
            data['skin'],
            data['hair']
        ]))

    homeless_repository.create_homeless(data)

    notification_service.new_homeless.delay(data={
        "_id": str(data["_id"]),
        "photo": data["photo"],
        "name": data["name"],
        "birth_date": date_to_string(data["birth_date"])
    })
