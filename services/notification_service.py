import configuration.telegram_configuration as telegram
from commons.templates.telegram_template import NEW_HOMELESS
from commons.templates.user_templates import SIGHTED_MISSING, REMEMBER_PASSWORD
from configuration.celery_configuration import celery
from configuration.loggin_configuration import logger
from configuration.send_grid_configuration import send_html

SUBJECT = "Desaparecidos.me - {}"


@celery.task()
def remember_password(new_password, email):
    try:
        template = REMEMBER_PASSWORD.format(new_password)
        send_html(email, SUBJECT.format("Senha recuperada"), template)
        logger.info("Send message to user %s", email)
    except Exception as e:
        logger.error("Error during send message", e)


@celery.task()
def sighted_missing(observation, email):
    try:
        template = SIGHTED_MISSING.format(observation)
        send_html(email, SUBJECT.format("Avistado"), template)
        logger.info("Send message to user %s", email)
    except Exception as e:
        logger.error("Error during send message", e)


@celery.task()
def new_homeless(data):
    try:
        message = NEW_HOMELESS.format(data["photo"],
                                      "Quero ser encontrado",
                                      data["name"],
                                      data["birth_date"],
                                      data["_id"])

        telegram.send_message_markdown_format(message)

        logger.info("Send message in telegram")
    except Exception as e:
        logger.error("Error during send message", e)
