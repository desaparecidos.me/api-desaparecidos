from datetime import datetime

from bson import ObjectId

import repositories.disappeared_repository as disappeared_repository
import repositories.sighted_repository as sighted_repository
import repositories.user_repository as user_repository
import services.notification_service as notification_service


def create_sighted(_id, data):
    data["disappeared_id"] = _id
    data["registration_date"] = datetime.now()
    sighted_repository.create_sighted(data)
    disappeared = disappeared_repository.find_by_id(ObjectId(_id))
    user = user_repository.find_by_id(ObjectId(disappeared["user_id"]))
    notification_service.sighted_missing.delay(data["observation"], user["email"])


def get_sighted_by_missing_id(_id):
    return [sighted_repository.json(item) for item in sighted_repository.get_sighted_by_missing_id(_id)]


def find_by_id(_id):
    return sighted_repository.json(sighted_repository.find_by_id(ObjectId(_id)))
