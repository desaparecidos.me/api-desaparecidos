from uuid import uuid4

from bson import ObjectId

import repositories.user_repository as repository
import services.notification_service as notification_service
from commons.hash import md5


def find_by_id(_id):
    return repository.json(repository.find_by_id(ObjectId(_id)))


def has_id(_id):
    return repository.find_by_id(ObjectId(_id)) is not None


def create_user(data):
    data["password"] = md5(data["password"])
    return repository.create_user(data)


def update_user(_id, update):
    user = repository.find_by_id(ObjectId(_id))
    user["name"] = update["name"]
    user["email"] = update["email"]
    user["phone"] = update["phone"]
    user["cell_phone"] = update["cell_phone"]
    user["avatar"] = update["avatar"]
    return repository.create_user(user)


def delete_user(_id):
    repository.delete_user(ObjectId(_id))


def get_all():
    return [repository.json(item) for item in repository.get_all()]


def update_password(_id, update):
    user = repository.find_by_id(ObjectId(_id))
    user["password"] = md5(update["password"])
    return repository.create_user(user)


def auth(email, password):
    return repository.json(repository.find_by_email_and_password(email, md5(password)))


def find_by_email(email):
    return repository.find_by_email(email)


def remember_password(user):
    new_password = str(uuid4())[0:5]
    user["password"] = md5(new_password)
    repository.create_user(user)
    notification_service.remember_password.delay(new_password, user["email"])
