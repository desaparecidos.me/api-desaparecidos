#!/usr/bin/env bash
pip install virtualenv
echo "Create virtualenv"
virtualenv venv
source venv/bin/activate
echo "Install requirements"
pip install -r requirements.txt

