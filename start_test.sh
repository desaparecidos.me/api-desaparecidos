#!/usr/bin/env bash
export MONGODB_URI=mongodb://localhost:27017/api-desaparecidos-test
export REDIS_URL=redis://localhost:6379/0
coverage run --omit=\"/Applications/PyCharm.app/Contents/helpers*\" -m unittest discover -s test -t test
coverage xml