from datetime import datetime


def missing_db():
    return {
        "name": "Alberto de Souza",
        "nickname": "beto",
        "height": "",
        "clothes": "Tenis calça jeans e moletom",
        "gender": "male",
        "eyes": "blue",
        "birth_date": datetime.now(),
        "event_report": "010101",
        "hair": "black",
        "skin": "white",
        "geometry": [
            "-46.870309",
            "-23.492381"
        ],
        "date_of_disappearance": datetime.now(),
        "status": "disappeared",
        "tattoo_description": "",
        "scar_description": "",
        "photo": "http://res.cloudinary.com/leandro-costa/image/upload/v1535409084/cafikbutjtesi5qbvofg.jpg",
        "user_id": "5b84a98ab8ea9280995f821f",
        "scar": True,
        "tattoo": True,
        "was_child": True,
        "slug": "5b84a98ab8ea9280995f821f-alberto-de-souza-built-in-method-date-of-datetime-datetime-object-at-0x104c911c0-blue-white-black"
    }


def get_missing():
    return {
        "name": "Alberto de Souza",
        "nickname": "beto",
        "height": "",
        "user_id": "",
        "clothes": "Tenis calça jeans e moletom",
        "gender": "male",
        "eyes": "blue",
        "birth_date": "01/01/1993",
        "event_report": "010101",
        "hair": "black",
        "skin": "white",
        "geometry": [
            "-46.870309",
            "-23.492381"
        ],
        "date_of_disappearance": "01/01/1993",
        "status": "disappeared",
        "tattoo_description": "",
        "scar_description": "",
        "photo": "http://res.cloudinary.com/leandro-costa/image/upload/v1535409084/cafikbutjtesi5qbvofg.jpg"
    }


def sighted():
    return {
        "observation": "Estava dormindo na rua",
        "geometry": [
            "-46.870309",
            "-23.492381"
        ]
    }
