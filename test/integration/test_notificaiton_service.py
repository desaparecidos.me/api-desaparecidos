import os
from unittest import TestCase

import services.notification_service as notification_service


class NotificationServiceTest(TestCase):

    def test_remember_password_exception(self):
        os.environ["SENDGRID_API_KEY"] = 'aaaa'
        with self.assertRaises(Exception) as e:
            notification_service.remember_password('aaaa', 'teste@gmail.com')
            self.assertEqual("Error during send message", e.exception)

    def test_sighted_missing_exception(self):
        os.environ["SENDGRID_API_KEY"] = 'aaaa'
        with self.assertRaises(Exception) as e:
            notification_service.sighted_missing("aaaa", "teste@gmail.com")
            self.assertEqual("Error during send message", e.exception)

    def test_new_homeless_exception(self):
        os.environ["SENDGRID_API_KEY"] = 'aaaa'
        with self.assertRaises(Exception) as e:
            data = {
                "photo": "aaaa",
                "name": "teste",
                "birth_date": "",
                "_id": 12313
            }
            notification_service.new_homeless(data)
            self.assertEqual("Error during send message", e.exception)
