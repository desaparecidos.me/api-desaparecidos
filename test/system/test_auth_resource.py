from test.contracts.users import get_user, auth
from test.test_base import TestBase


class AuthResourceTest(TestBase):

    def test_authentication(self):
        with self.app_context():
            user = get_user()
            result = self.app.post("/users", data=user, headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            result = self.app.post("/auth", data=auth(), headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, result.status_code)

    def test_authentication_without_field(self):
        with self.app_context():
            a = auth()
            a.pop("email")

            result = self.app.post("/auth", data=a, headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(400, result.status_code)

    def test_authentication_when_user_not_registered(self):
        with self.app_context():
            a = auth()
            result = self.app.post("/auth", data=a, headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(404, result.status_code)
