from configuration.mongodb_configuration import database
from test.contracts.missing import get_missing
from test.contracts.users import get_user
from test.test_base import TestBase


class DisappearedListResource(TestBase):

    def test_get_all_from_pagination(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            pagination = self.app.get('/disappeared?page=0',
                                      headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, pagination.status_code)

            self.assertEqual(1, len(pagination.json["disappeared"]))

    def test_get_all_from_count(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            count = self.app.get('/disappeared?count', headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, count.status_code)

            self.assertEqual(1, int(count.json["count"]))

    def test_search_all_missing_with_full_in_scan_document(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            search_result = self.app.get('/disappeared?search={}'.format(missing["eyes"]),
                                         headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, search_result.status_code)
            self.assertEqual(1, len(search_result.json["disappeared"]))

    def test_get_all_geometry(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            search_result = self.app.get('/disappeared?geometry',
                                         headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, search_result.status_code)
            self.assertEqual(1, len(search_result.json["disappeared"]))

    def test_get_info(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            info_result = self.app.get('/disappeared?info',
                                       headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, info_result.status_code)
            self.assertTrue(info_result.json["disappeared"]["gender"]["male"] > 0)
