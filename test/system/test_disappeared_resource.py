from configuration.mongodb_configuration import database
from test.contracts.missing import get_missing
from test.contracts.users import get_user, get_id
from test.test_base import TestBase


class MissingResourceTest(TestBase):

    def test_create_missing(self):
        with self.app_context():
            user = get_user()
            missing = get_missing()

            database.db.users.save(user)
            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            missing_result = self.app.get(result.json["uri"],
                                          headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, missing_result.status_code)
            self.assertIsNotNone(missing_result.json)

            self.assertEqual(missing["name"], missing_result.json["missing"]["name"])

    def test_create_missing_when_user_not_found(self):
        with self.app_context():
            missing = get_missing()

            result = self.app.post("/user/{}/disappeared".format(get_id()), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found"}, result.json)

    def test_create_missing_when_has_registered(self):
        with self.app_context():
            user = get_user()
            missing = get_missing()

            database.db.users.save(user)
            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)
            self.assertTrue("uri" in result.json)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(409, result.status_code)
            self.assertDictEqual({"message": "Error conflict, missing has registered!"}, result.json)

    def test_create_with_insufficient_fields(self):
        with self.app_context():
            user = get_user()
            missing = get_missing()

            database.db.users.save(user)

            missing["hair"] = "preto"

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            missing["hair"] = "black"

            self.assertEqual(400, result.status_code)
            self.assertDictEqual({"message": "Request error fields required"}, result.json)

    def test_get_all_missing(self):
        with self.app_context():
            user = get_user()
            missing = get_missing()

            database.db.users.save(user)

            result = self.app.post("/user/{}/disappeared".format(str(user["_id"])), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)
            self.assertTrue("uri" in result.json)

            result = self.app.get("/user/{}/disappeared".format(str(user["_id"])),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)
            self.assertEqual(1, len(result.json["disappeared"]))

    def test_get_all_missing_when_user_not_found(self):
        with self.app_context():
            missing = get_missing()

            result = self.app.post("/user/{}/disappeared".format(get_id()), data=missing,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found"}, result.json)
