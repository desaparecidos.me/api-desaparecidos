from test.contracts.homeless import get_homeless
from test.contracts.users import get_id
from test.test_base import TestBase


class HomelessResourceTest(TestBase):

    def test_create_homeless(self):
        with self.app_context():
            homeless = get_homeless()
            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)
            homeless_result = self.app.get(result.json["uri"],
                                           headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(homeless["name"], homeless_result.json["homeless"]["name"])

    def test_create_homeless_conflict(self):
        with self.app_context():
            homeless = get_homeless()
            self.app.post("/homeless", data=homeless, headers={'Authorization': 'Basic ' + self.valid_credentials})

            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(409, result.status_code)
            self.assertDictEqual({"message": "Error conflict, homeless has registered!"}, result.json)

    def test_create_homeless_invalid_parameters(self):
        with self.app_context():
            homeless = get_homeless()
            homeless['eyes'] = 'red'
            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(400, result.status_code)
            self.assertEqual({"message": "Json malformed"}, result.json)

    def test_create_homeless_invalid_date(self):
        with self.app_context():
            homeless = get_homeless()
            homeless['birth_date'] = '2018/01/01'

            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            homeless['birth_date'] = '01/01/2014'

            self.assertEqual(400, result.status_code)

            self.assertEqual({"message": "Json malformed"}, result.json)

    def test_get_all_homeless(self):
        with self.app_context():
            homeless = get_homeless()
            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            result = self.app.get('/homeless', headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)

            self.assertEqual(1, len(result.json['homeless']))

    def test_get_all_homeless_empty_list(self):
        with self.app_context():
            result = self.app.get('/homeless', headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, result.status_code)
            self.assertEqual(0, len(result.json['homeless']))
            self.assertDictEqual({"homeless": []}, result.json)

    def test_get_homeless_by_id_when_not_found(self):
        with self.app_context():
            result = self.app.get('/homeless/{}'.format(get_id()),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"homeless": {}}, result.json)

    def test_get_info_homeless(self):
        with self.app_context():
            homeless = get_homeless()
            result = self.app.post("/homeless", data=homeless,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            result = self.app.get('/homeless?info', headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)

            self.assertTrue(result.json['homeless']["gender"]["male"] > 0)
