from configuration.mongodb_configuration import database
from test.contracts.missing import missing_db, get_missing
from test.contracts.users import get_id, get_user
from test.test_base import TestBase


class MissingResourceTest(TestBase):

    def test_get_missing_by_id(self):
        with self.app_context():
            mdb = missing_db()
            database.db.disappeared.save(mdb)

            result = self.app.get('/missing/{}'.format(mdb["_id"]),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)
            self.assertIsNotNone(result.json["missing"])

    def test_get_missing_when_id_not_found(self):
        with self.app_context():
            result = self.app.get('/missing/{}'.format(get_id()),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "Missing not found!"}, result.json)

    def test_get_missing_server_exception(self):
        with self.app_context():
            mdb = missing_db()
            mdb.pop("birth_date")

            database.db.disappeared.save(mdb)

            result = self.app.get('/missing/{}'.format(str(mdb["_id"])),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(500, result.status_code)
            self.assertDictEqual({"message": "Error during get missing"}, result.json)

    def test_remove_missing_by_id_when_missing_not_registered(self):
        with self.app_context():
            result = self.app.delete('/missing/{}'.format(get_id()),
                                     headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "Missing not found!"}, result.json)

    def test_remove_missing_by_id_when_id_malformed(self):
        with self.app_context():
            result = self.app.delete('/missing/{}'.format(1),
                                     headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(500, result.status_code)
            self.assertDictEqual({"message": "Error during delete missing"}, result.json)

    def test_remove_missing_by_id(self):
        with self.app_context():
            mdb = missing_db()
            database.db.disappeared.save(mdb)
            result = self.app.delete("/missing/{}".format(str(mdb["_id"])),
                                     headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(204, result.status_code)

            missing = database.db.disappeared.find_one({"_id": mdb["_id"]})

            self.assertIsNone(missing)

    def test_update_missing_when_send_invalid_id(self):
        with self.app_context():
            result = self.app.put('/missing/{}'.format(1), headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(500, result.status_code)
            self.assertDictEqual({"message": "Error during update missing"}, result.json)

    def test_update_missing_when_user_not_found(self):
        with self.app_context():
            missing = get_missing()
            result = self.app.put('/missing/{}'.format(get_id()), data=missing,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found!"}, result.json)

    def test_update_missing_when_missing_not_create(self):
        with self.app_context():
            user = get_user()

            missing = get_missing()

            database.db.users.save(user)

            missing["user_id"] = str(user["_id"])

            result = self.app.put('/missing/{}'.format(str(user["_id"])), data=missing,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            missing_result = self.app.get(result.json["uri"],
                                          headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, missing_result.status_code)

            self.assertEqual(missing["name"], missing_result.json["missing"]["name"])

    def test_update_missing_when_missing(self):
        with self.app_context():
            user = get_user()
            missing = get_missing()
            missing["name"] = "Update Missing"

            mdb = missing_db()

            database.db.users.save(user)

            mdb["user_id"] = str(user["_id"])

            database.db.disappeared.save(mdb)

            missing["user_id"] = str(user["_id"])

            result = self.app.put('/missing/{}'.format(str(mdb["_id"])), data=missing,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)

            result_missing = self.app.get(result.json["uri"],
                                          headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(missing["name"], result_missing.json["missing"]["name"])
