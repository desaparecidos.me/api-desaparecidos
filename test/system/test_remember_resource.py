from configuration.mongodb_configuration import database
from test.contracts.users import get_user, remember_password
from test.test_base import TestBase


class RememberResourceTest(TestBase):

    def test_remember_password_when_user_not_found(self):
        with self.app_context():
            result = self.app.patch("/auth/remember", data={"email": "teste@gmail.com"},
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found!"}, result.json)

    def test_remember_password_when_malformed_json(self):
        with self.app_context():
            result = self.app.patch("/auth/remember", data={"email_": "teste@gmail.com"},
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(400, result.status_code)
            self.assertDictEqual({"message": "Json malformed to remember password"}, result.json)

    def test_remember_password(self):
        with self.app_context():
            user = get_user()
            remember = remember_password()

            database.db.users.save(user)
            result = self.app.patch("/auth/remember", data=remember,
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)
