from configuration.mongodb_configuration import database
from test.contracts.missing import sighted, missing_db
from test.contracts.users import get_id, get_user
from test.test_base import TestBase


class SightedResourceTest(TestBase):

    def test_inform_sighted_when_missing_not_found(self):
        with self.app_context():
            s = sighted()
            result = self.app.post("/missing/{}/sighted".format(get_id()), data=s,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "Missing not found"}, result.json)

    def test_inform_sighted(self):
        with self.app_context():
            user = get_user()
            missing = missing_db()
            s = sighted()

            database.db.users.save(user)
            missing["user_id"] = user["_id"]

            database.db.disappeared.save(missing)
            result = self.app.post("/missing/{}/sighted".format(str(missing["_id"])), data=s,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            sighted_result = self.app.get(result.json["uri"],
                                          headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, sighted_result.status_code)

            self.assertEqual(s.get("observation"), sighted_result.json["sighted"]["observation"])

    def test_inform_sighted_exception(self):
        with self.app_context():
            s = sighted()
            result = self.app.post("/missing/{}/sighted".format(1), data=s,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(500, result.status_code)
            self.assertDictEqual({"message": "Error during sighted missing"}, result.json)

    def test_get_all_sighted(self):
        with self.app_context():
            user = get_user()
            missing = missing_db()
            s = sighted()

            database.db.users.save(user)

            missing["user_id"] = user["_id"]

            database.db.disappeared.save(missing)

            result = self.app.post("/missing/{}/sighted".format(str(missing["_id"])), data=s,
                                   headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            result = self.app.get("/missing/{}/sighted".format(str(missing["_id"])), data=s,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)
            self.assertEqual(1, len(result.json["sighted"]))

    def test_get_sighted_when_missing_not_found(self):
        with self.app_context():
            result = self.app.get("/missing/{}/sighted".format(get_id()),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "Missing not found"}, result.json)

    def test_get_sighted_when_missing_exception(self):
        with self.app_context():
            result = self.app.get("/missing/{}/sighted".format(1),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(500, result.status_code)
            self.assertDictEqual({"message": "Error during get sighted missing"}, result.json)

    def test_get_sighted_by_id_when_id_not_found(self):
        with self.app_context():
            result = self.app.get("/sighted/{}".format(get_id()),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"sighted": {}}, result.json)
