from configuration.mongodb_configuration import database
from test.contracts.users import get_user, get_id
from test.test_base import TestBase


class UserResourceTest(TestBase):

    def test_get_user_by_id(self):
        with self.app_context():
            user = get_user()
            database.db.users.save(user)
            result = self.app.get('/user/{}'.format(str(user["_id"])),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, result.status_code)
            email = result.json["user"]["email"]
            self.assertEqual(user["email"], email)

    def test_error_invalid_id(self):
        with self.app_context():
            result = self.app.get('/user/1', headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(400, result.status_code)
            self.assertDictEqual({"message": "Error the id {}, informed is invalid".format(1)}, result.json)

    def test_find_user_not_found(self):
        with self.app_context():
            result = self.app.get("/user/{}".format(get_id()),
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(404, result.status_code)
            self.assertDictEqual({}, result.json["user"])

    def test_update_user_when_data_with_insufficient_field(self):
        with self.app_context():
            user = get_user()
            user.pop("name")

            result = self.app.put("/user/{}".format(get_id()), data=user,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(400, result.status_code)
            self.assertEqual({"message": "Request error fields required"}, result.json)

            user["name"] = "Leandro Costa"

    def test_update_user_when_user_not_created(self):
        with self.app_context():
            user = get_user()
            result = self.app.put("/user/{}".format(get_id()), data=user,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(201, result.status_code)

            user_result = self.app.get(result.json["uri"], headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, user_result.status_code)

            self.assertEqual(user["email"], user_result.json["user"]["email"])

    def test_update_user_when_user_not_created_conflict(self):
        with self.app_context():
            user = get_user()
            self.app.put("/user/{}".format(get_id()), data=user,
                         headers={'Authorization': 'Basic ' + self.valid_credentials})
            result = self.app.put("/user/{}".format(get_id()), data=user,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(409, result.status_code)
            self.assertEqual({"message": "Error conflict, email has registered!"}, result.json)

    def test_update_user(self):
        with self.app_context():
            user = get_user()
            database.db.users.save(user)
            user["email"] = "teste@gmail.com"

            result = self.app.put("/user/{}".format(str(user["_id"])), data=user,
                                  headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, result.status_code)

            user_result = self.app.get(result.json["uri"], headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, user_result.status_code)

            self.assertEqual(user["email"], user_result.json["user"]["email"])

    def test_remove_user_by_id(self):
        with self.app_context():
            user = get_user()
            database.db.users.save(user)

            result = self.app.delete("/user/{}".format(str(user["_id"])),
                                     headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(204, result.status_code)

    def test_remove_user_by_id_when_user_not_found(self):
        with self.app_context():
            result = self.app.delete("/user/{}".format(get_id()),
                                     headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found!"}, result.json)

    def test_update_password(self):
        with self.app_context():
            user = get_user()
            database.db.users.save(user)
            result = self.app.patch("/user/{}".format(str(user["_id"])), data={"password": "abc1234"},
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(200, result.status_code)

    def test_update_password_when_user_not_found(self):
        with self.app_context():
            result = self.app.patch("/user/{}".format(get_id()), data={"password": "abc1234"},
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(404, result.status_code)
            self.assertDictEqual({"message": "User not found!"}, result.json)

    def test_update_password_when_data_is_empty(self):
        with self.app_context():
            result = self.app.patch("/user/{}".format(get_id()), data={},
                                    headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(400, result.status_code)
            self.assertDictEqual({"message": "Request error fields required"}, result.json)
