from test.contracts.users import get_user
from test.test_base import TestBase


class UsersResourcesTest(TestBase):

    def test_create_user(self):
        with self.app_context():
            user = get_user()
            result = self.app.post('/users', data=user, headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(201, result.status_code)

            result = self.app.get(result.json["uri"], headers={'Authorization': 'Basic ' + self.valid_credentials})
            self.assertEqual(user["email"], result.json["user"]["email"])

    def test_create_user_conflict(self):
        with self.app_context():
            user = get_user()
            self.app.post('/users', data=user, headers={'Authorization': 'Basic ' + self.valid_credentials})
            result = self.app.post('/users', data=user, headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(409, result.status_code)
            self.assertDictEqual({"message": "Error conflict, email was registered!"}, result.json)

    def test_get_all_user(self):
        with self.app_context():
            user = get_user()
            self.app.post('/users', data=user, headers={'Authorization': 'Basic ' + self.valid_credentials})

            result = self.app.get('/users', headers={'Authorization': 'Basic ' + self.valid_credentials})

            self.assertEqual(200, result.status_code)
            self.assertEqual(1, len(result.json["users"]))
