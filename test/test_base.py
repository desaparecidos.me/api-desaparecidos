import base64
import os
from unittest import TestCase

from app import app
from configuration.mongodb_configuration import database


class TestBase(TestCase):
    username = os.environ.get('BASIC_AUTH_USERNAME')
    password = os.environ.get('BASIC_AUTH_PASSWORD')

    valid_credentials = base64.b64encode("{}:{}".format(username, password).encode()).decode('utf-8')

    def setUp(self):
        app.testing = True
        app.config["MONGO_URI"] = os.environ.get('MONGODB_URI')
        app.config['CELERY_BROKER_URL'] = os.environ.get('REDIS_URL')
        app.config['CELERY_RESULT_BACKEND'] = os.environ.get('REDIS_URL')
        with app.app_context():
            database.init_app(app)

        self.app = app.test_client()
        self.app_context = app.app_context

    def tearDown(self):
        with app.app_context():
            database.db.users.delete_many({})
            database.db.disappeared.delete_many({})
            database.db.locations.delete_many({})
            database.db.homeless.delete_many({})
            database.db.sighted.delete_many({})
