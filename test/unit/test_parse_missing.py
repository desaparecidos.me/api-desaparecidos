from unittest import TestCase

import parser.missing_parse as missing_parse


class ParseMissingTest(TestCase):

    def test_height_exception(self):
        with self.assertRaises(ValueError) as context:
            missing_parse.height("1,1")

            self.assertEqual("Invalid format height", context.exception)

    def test_height_parse(self):
        result = missing_parse.height("1.80")

        self.assertEqual("1.80", result)

    def test_format_date(self):
        with self.assertRaises(ValueError) as context:
            missing_parse.format_date("2018/01/01")

            self.assertEqual("Invalid format to birth date", context.exception)
